package be.persgroep.adv.generic.automapper;

import be.persgroep.adv.generic.automapper.model.Person;
import be.persgroep.adv.generic.automapper.model.PersonObjectMother;
import be.persgroep.adv.generic.automapper.viewmodel.PersonViewModel;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: mcautreels
 * Date: 3/03/13
 * Time: 22:04
 */
public class AbstractObjectMapperTest {
    @Test
    public void testConvertObject() throws Exception {
        Person person = PersonObjectMother.getPerson();

        PersonViewModel personViewModel = (PersonViewModel) AbstractObjectMapper.convertObject(person, PersonViewModel.class);
        assertNotNull(personViewModel);
        assertEquals(person.getId(), personViewModel.getId());
        assertEquals(person.getFirstName(), personViewModel.getFirstName());
    }

    @Test
    public void testConvertObjectList() throws Exception {
        Person person1 = PersonObjectMother.getPerson();
        Person person2 = PersonObjectMother.getPerson2();

        List<Person> persons = new ArrayList<Person>();
        persons.add(person1);
        persons.add(person2);

        List<PersonViewModel> personViewModels = AbstractObjectMapper.convertObjectList(persons, PersonViewModel.class);

        PersonViewModel personViewModel = personViewModels.get(0);

        assertNotNull(personViewModel);
        assertEquals(person1.getId(), personViewModel.getId());
        assertEquals(person1.getFirstName(), personViewModel.getFirstName());

        PersonViewModel personViewModel2 = personViewModels.get(1);

        assertNotNull(personViewModel2);
        assertEquals(person2.getId(), personViewModel2.getId());
        assertEquals(person2.getFirstName(), personViewModel2.getFirstName());
    }
}
