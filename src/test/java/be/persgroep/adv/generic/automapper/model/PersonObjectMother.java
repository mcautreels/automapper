package be.persgroep.adv.generic.automapper.model;

import java.util.Calendar;

/**
 * User: mcautreels
 * Date: 29/04/13
 * Time: 11:31
 */
public class PersonObjectMother {
    public static Person getPerson() {
        Person person = new Person();

        person.setId(1L);
        person.setFirstName("Test");
        person.setLastName("Person");
        person.setEmail("test.person@persgroep.be");

        Calendar cal = Calendar.getInstance();
        cal.set(1990, Calendar.FEBRUARY, 25);
        person.setDateOfBirth(cal.getTime());

        person.setPassword("bloemkool");

        return person;
    }

    public static Person getPerson2() {
        Person person = new Person();

        person.setId(1L);
        person.setFirstName("Test2");
        person.setLastName("Person2");
        person.setEmail("test2.person2@persgroep.be");

        Calendar cal = Calendar.getInstance();
        cal.set(1990, Calendar.FEBRUARY, 25);
        person.setDateOfBirth(cal.getTime());

        person.setPassword("brocoli");

        return person;

    }
}
