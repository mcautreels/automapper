package be.persgroep.adv.generic.automapper;

import be.persgroep.adv.generic.automapper.model.Person;
import be.persgroep.adv.generic.automapper.model.PersonObjectMother;
import be.persgroep.adv.generic.automapper.viewmodel.PersonViewModel;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * User: mcautreels
 * Date: 29/04/13
 * Time: 11:39
 */
public class PersonMapperTest {

    PersonMapper personMapper = new PersonMapper();

    @Test
    public void toViewModelTest() throws InstantiationException, IllegalAccessException {
        Person person = PersonObjectMother.getPerson();

        PersonViewModel personViewModel = personMapper.toViewModel(person);
        assertNotNull(personViewModel);
        assertEquals(person.getFirstName(), personViewModel.getFirstName());
        assertEquals(person.getEmail().replace("@", "[at]"), personViewModel.getSpamSafeEmail());
    }
}
