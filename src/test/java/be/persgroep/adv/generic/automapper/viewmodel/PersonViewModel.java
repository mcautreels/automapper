package be.persgroep.adv.generic.automapper.viewmodel;

import java.util.Date;

/**
 * User: mcautreels
 * Date: 29/04/13
 * Time: 11:24
 */
public class PersonViewModel extends AbstractViewModel {

    private Long id;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String spamSafeEmail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSpamSafeEmail() {
        return spamSafeEmail;
    }

    public void setSpamSafeEmail(String spamSafeEmail) {
        this.spamSafeEmail = spamSafeEmail;
    }
}
