package be.persgroep.adv.generic.automapper;

import be.persgroep.adv.generic.automapper.model.DatabaseEntity;
import be.persgroep.adv.generic.automapper.viewmodel.AbstractViewModel;

import java.util.Collection;

/**
 * The abstract mapper class to bind database models with view models.
 *
 * @param <M> the DatabaseEntity type
 * @param <V> the AbstractViewModel type
 *            <p/>
 *            User: vsoltys
 *            Date: 3/10/13
 *            Time: 9:01 PM
 */
public abstract class AbstractViewModelMapper<M extends DatabaseEntity, V extends AbstractViewModel> {

    /**
     * Transforms database model to view model
     *
     * @param model database model to transform into view model
     * @return mapped view model
     * @throws IllegalAccessException, InstantiationException
     */
    public abstract V toViewModel(M model) throws IllegalAccessException, InstantiationException;

    /**
     * Transforms collection of database models to view models
     *
     * @param models Collection of database models to transform into view models
     * @return Collection of mapped view models
     * @throws IllegalAccessException, InstantiationException
     */
    public abstract Collection<V> toViewModels(Collection<M> models) throws IllegalAccessException, InstantiationException;

    /**
     * Transforms view model to database model
     *
     * @param viewModel view model to transform into database model
     * @return mapped database model
     * @throws IllegalAccessException, InstantiationException
     */
    public abstract M toModel(V viewModel) throws IllegalAccessException, InstantiationException;

    /**
     * Transforms collection of view models to database models
     *
     * @param viewModels Collection of view models to transform into database models
     * @return Collection of mapped database models
     * @throws IllegalAccessException, InstantiationException
     */
    public abstract Collection<M> toModels(Collection<V> viewModels) throws IllegalAccessException, InstantiationException;
}
